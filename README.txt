Views Infinite Scroll with background image
-------
This module allows you to use Views Infinite Scroll module with 
Image URL formatter for refresh background images of new loaded elements.

For safety reasons, Views module doesn't allow code, like 
<div style="background: url([field_image]) no-repeat;">...</div> 
on view. Therefore, it's impossible to update items that have appeared.

Requirements:

Views Infinite Scroll [1]
Image URL formatter [2]


Standard usage scenario
--------------------------------------------------------------------------------
3. Download module, upload to server (./sites/all/modules).
4. Go to administration page (admin/modules) and enable module.
5. Go to module's settings page 
(admin/config/content/views_infinite_scroll_bg_image/settings).
6. Follow instructions.


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Victor Shostak [3].

Ongoing development is sponsored by IA Central marketing [4] and Blizzy's 
Blog [5].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://github.com/enjoyiacm/telegram_bot_notification/issues


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/project/views_infinite_scroll
2: https://www.drupal.org/project/image_url_formatter
3: https://www.drupal.org/u/enjoyiacm
4: http://centralmarketing.ru
5: http://blizzy.ru
