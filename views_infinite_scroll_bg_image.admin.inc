<?php
/**
 * @file
 * Administration interfaces.
 */

/**
 * Main settings of Views Infinite Scroll with background image.
 */
function views_infinite_scroll_bg_image_settings() {
  // Help info.
  $form['views_infinite_scroll_bg_image']['help_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Help info'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 0,
    '#description' => t('<p>To work correctly, configure your view (Views 3.x) like this:</p>
                      <ol>
                        <li>Add image field to view (ex. name <code>field_image</code>);</li>
                        <li>At format column, choose «Image URL» instead of «Image»;</li>
                        <li>Add «Global: Custom text» field to view;</li>
                        <li>At this field, add your desing for row;</li>
                        <li>Put attribute <code>data-bg-image</code> with token <code>[field_image]</code> to element you want to specify a background image (ex. <code>&lt;div class="my-class" data-bg-image="[field_image]"&gt;...&lt;/div&gt;</code>);</li>
                        <li>Save view.</li>
                      </ol>
                      <p>If you looking for screencast, see <a href="http://blizzy.ru/node/23">this blog post</a>.</p>'),
  );

  // Specify pair.
  $form['views_infinite_scroll_bg_image']['specify_element'] = array(
    '#type' => 'fieldset',
    '#title' => t('Specify pair classes (ID) with background-size'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 1,
  );
  $form['views_infinite_scroll_bg_image']['specify_element']['element_settings'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('views_infinite_scroll_bg_image_element_settings', '.view .views-row|cover'),
    '#description' => t('<p>Enter one pair in each line. Use separator <strong>|</strong> for split class and <code>background-size</code> setting. Supported property <code>cover</code> and <code>contain</code>. For example:</p>
                      <ul>
                        <li>.item<strong>|</strong>cover</li>
                        <li>.wrapper .row .column<strong>|</strong>contain</li>
                        <li>#container #item<strong>|</strong>cover</li>
                      </ul>'),
  );

  // Return form.
  return system_settings_form($form);
}
