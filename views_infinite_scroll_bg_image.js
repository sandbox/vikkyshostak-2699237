/**
 * @file
 * Script file of Views Infinite Scroll with background image.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.views_infinite_scroll_bg_image = {
    attach: function (context, settings) {
      // Define variables.
      var index;
      var element = $(Drupal.settings.views_infinite_scroll_bg_image);
      // Reload function.
      function reload(index) {
        // Once load.
        $(element[index]['elementClass'], context).once(function () {
          $(this).css({
            'background-image': 'url(' + $(this).data('bg-image') + ')',
            'background-repeat': 'no-repeat',
            'background-position': 'center'
          });
          // Check background-size property.
          if (element[index]['backgroundSize'] === 'cover') {
            $(this).css({
              // For new browsers.
              'background-size': 'cover',
              // For old browsers.
              '-webkit-background-size': 'cover',
              '-khtml-background-size': 'cover',
              '-moz-background-size': 'cover',
              '-o-background-size': 'cover',
              // For IE8.
              'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + $(this).data('bg-image') + '", sizingMethod="scale")',
              '-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + $(this).data('bg-image') + '", sizingMethod="scale")'
            });
          }
          else if (element[index]['backgroundSize'] === 'contain') {
            $(this).css({
              // For new browsers.
              'background-size': 'contain',
              // For old browsers.
              '-webkit-background-size': 'contain',
              '-khtml-background-size': 'contain',
              '-moz-background-size': 'contain',
              '-o-background-size': 'contain',
              // For IE8.
              'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + $(this).data('bg-image') + '", sizingMethod="scale")',
              '-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + $(this).data('bg-image') + '", sizingMethod="scale")'
            });
          }
        });
      }
      // Check is empty.
      if (element.length > 0) {
        // Get all elements.
        for (index = 0; index < element.length; ++index) {
          reload(index);
        }
      }
    }
  };
})(jQuery, Drupal);
